# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Purpose License v2

# TODO move cmake requirement to webkit.exlib once webkit:3.0 is gone
require cmake [ api=2 cmake_minimum_version=3.3 ] webkit [ required_gcc='5.5' ]
require option-renames [ renames=[ 'gtk2 providers:gtk2' ] ]

SLOT="4.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    hyphen [[ description = [ Enable hyphenation using libhyphen ] ]]
    libnotify
    nsplugin [[ description = [ Build the Netscape plugin API ( required for nsplugin support) ] ]]
    opengl [[ description = [ Enable OpenGL & WebGL support ] ]]
    spell
    wayland [[ requires = [ opengl ] ]]
    ( providers: gtk2 [[ description = [ Support gtk2 plugins. Most plugins (e.g. flash) need this ] ]] )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: ar as bg cs de el en_CA en_GB eo es et eu fr gl gu he hi hu id it kn ko lt lv ml mr
               nb nl or pa pl pt pt_BR ro ru sl sr sr@latin sv ta te uk vi zh_CN )
    ( platform: amd64 )
"

RESTRICT="test" # requires X

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.10.0]
        dev-lang/python:*[~>2.7.0]
        dev-lang/ruby:*[>=1.9]
        dev-util/gperf[>=3.0.1]
        sys-devel/gettext
        sys-devel/ninja
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-db/sqlite:3[>=3.0]
        dev-libs/at-spi2-core[>=2.6.2]
        dev-libs/atk
        dev-libs/glib:2[>=2.44.0] [[ note = [ 2.44 for glib networking ] ]]
        dev-libs/icu:=
        dev-libs/libgcrypt[>=1.7.0]
        dev-libs/libtasn1
        dev-libs/libsecret:1
        dev-libs/libxml2:2.0[>=2.8.0]
        dev-libs/libxslt[>=1.1.7]
        gnome-desktop/libsoup:2.4[>=2.49.1.1][gobject-introspection?]
        gps/geoclue:2.0[>=2.1.5]
        media-libs/fontconfig[>=2.8.0]
        media-libs/freetype:2[>=2.4.2]
        media-libs/libpng:=[>=1.2]
        media-libs/libwebp:=
        sys-libs/zlib
        x11-dri/mesa[wayland?] [[ note = [ provides EGL, OpenGL, OpenGLES2 ] ]]
        x11-libs/cairo[>=1.14.4][X] [[ note = [ X required for cairo-gl for CSS support ] ]]
        x11-libs/gtk+:3[>=3.14.0][gobject-introspection?][wayland?]
        x11-libs/harfbuzz[>=1.3.3]

        (
            x11-libs/libX11
            x11-libs/libXcomposite
            x11-libs/libXdamage
            x11-libs/libXrender
            x11-libs/libXt
        ) [[ note = [ required for the X11 backend ] ]]

        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
        hyphen? ( office-libs/hyphen )
        libnotify? ( x11-libs/libnotify )
        (
            media-libs/gstreamer:1.0[>=1.10]
            media-plugins/gst-plugins-bad:1.0[>=1.10] [[ note = [ for mpegts ] ]]
            media-plugins/gst-plugins-base:1.0[>=1.10] [[ note = [ for fft ] ]]
            media-plugins/gst-plugins-good:1.0[>=1.10][gstreamer_plugins:taglib]
        ) [[ note = [ Required for HTML5 audio/video, disabling this is known to cause compilation errors ] ]]
        opengl? (
            media-plugins/gst-plugins-base:1.0[>=1.8.0][gstreamer_plugins:opengl][gstreamer_plugins:eglgles]
        )
        providers:gtk2? ( x11-libs/gtk+:2[>=2.24.10] )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        spell? ( app-spell/enchant:=[>=0.22] )
        wayland? ( sys-libs/wayland )
"

CMAKE_SOURCE="${WORKBASE}/webkitgtk-${PV}/"

# ENABLE_DATA_TRANSFER_ITEMS
# ENABLE_INPUT_TYPE_COLOR
# ENABLE_INPUT_TYPE_DATE
# ENABLE_INPUT_TYPE_DATETIMELOCAL
# ENABLE_INPUT_TYPE_MONTH
# ENABLE_INPUT_TYPE_TIME
# ENABLE_INPUT_TYPE_WEEK
# ENABLE_MEDIA_STATISTICS
# ENABLE_SCRIPTED_SPEECH
# ENABLE_TEXT_AUTOSIZING
# ENABLE_WEBASSEMBLY
#   disabled by default upstream
# USE_SYSTEM_MALLOC
#   disabled by default upstream
#   NOTE(compnerd) DO NOT PROVIDE AN OPTION FOR THIS (bmalloc is only disabled on android)
# ENABLE_CSS_COMPOSITING
# ENABLE_DEVICE_ORIENTATION
# ENABLE_ORIENTATION_EVENTS
#   only enabled on iOS builds
# ENABLE_MOUSE_CURSOR_SCALE
# ENABLE_TOUCH_SLIDER
#   only enabled on enlightenment builds
#   TODO(compnerd) determine what these do
# ENABLE_FTPDIR
# ENABLE_INDEXED_DATABASE_IN_WORKERS
#   disabled by default in GTK+ port
# ENABLE_WEBGPU
#   New alternative to WebGL, experimental
# ENABLE_WEBGL2
#   fails to build with 2.18.0
# DENABLE_MEDIA_STREAM:BOOL=OFF
# DENABLE_WEB_RTC:BOOL=OFF
#   Require unpackaged OpenWebRTC

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DENABLE_VIDEO:BOOL=ON
    -DENABLE_WEB_AUDIO:BOOL=ON
    -DENABLE_WEB_CRYPTO:BOOL=ON
    -DENABLE_WEBDRIVER:BOOL=ON
    -DENABLE_XSLT:BOOL=ON
    -DPORT=GTK
    -DUSE_LD_GOLD:BOOL=OFF
    -DUSE_LIBSECRET:BOOL=ON

    # Needs unpackaged woff2
    -DUSE_WOFF2:BOOL=OFF
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection INTROSPECTION'
    'gtk-doc GTKDOC'
    'nsplugin NETSCAPE_PLUGIN_API'
    'opengl OPENGL'
    'opengl ACCELERATED_2D_CANVAS'
    'opengl WEBGL'
    'platform:amd64 FTL_JIT'
    'providers:gtk2 PLUGIN_PROCESS_GTK2'
    'spell SPELLCHECK'
    'wayland WAYLAND_TARGET'
)
CMAKE_SRC_CONFIGURE_OPTION_USES=(
    'hyphen LIBHYPHEN'
    'libnotify LIBNOTIFY'
)

src_prepare() {
    edo cd ${CMAKE_SOURCE}
    edo sed -i "s/pkg-config/${PKG_CONFIG}/" Source/WebCore/PlatformGTK.cmake
    webkit_src_prepare
}

src_install() {
    cmake_src_install
    edo mv "${IMAGE}"/usr/{$(exhost --target)/,}share/locale
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
}

