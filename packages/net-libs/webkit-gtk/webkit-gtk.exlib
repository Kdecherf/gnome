# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gsettings
require flag-o-matic

SUMMARY="Web Browser Engine based on WebKit (gtk2)"
HOMEPAGE="http://www.webkit.org/"

DOWNLOADS="http://webkitgtk.org/releases/webkitgtk-${PV}.tar.xz"
WORK="${WORKBASE}/webkitgtk-${PV}/"

LICENCES="LGPL-2 LGPL-2.1 BSD-3"

webkit-gtk_src_prepare() {
    # NOTE(compnerd) tweak compilation options for memory
    # This is particularly important after 1.10 as the build has gotten to the point where it
    # will start hitting 4GB limitations on 32-bit platforms otherwise

    # reducing the debug symbols to minimum makes a *significant* impact
    # ar has started hitting 4G limitations in certain cases
    # https://bugs.webkit.org/show_bug.cgi?id=91154
    [[ " ${CXXFLAGS} " == *\ -g* ]] && append-flags -g1

    # attempt to reduce memory usage during the linking phase
    append-ldflags -Wl,--no-keep-memory
    [[ $(eclectic ld show) == gold ]] || append-ldflags -Wl,--reduce-memory-overheads

    default
}

webkit-gtk_src_compile() {
    edo mkdir -p "${WORK}/DerivedSources/webkit"                        \
                 "${WORK}/DerivedSources/WebCore"                       \
                 "${WORK}/DerivedSources/ANGLE"                         \
                 "${WORK}/DerivedSources/WebKit2/webkit2gtk/webkit2"    \
                 "${WORK}/DerivedSources/InjectedBundle"
    default
}

webkit-gtk_src_install() {
    #Fix for cross/multiarch
    edo sed -e "s/'pkg-config'/'${PKG_CONFIG}'/g" -i Tools/gtk/generate-gtkdoc -i Tools/gtk/common.py -i Tools/gtk/gtkdoc.py
    gsettings_src_install
    edo find "${IMAGE}" -depth -type d -empty -delete
}

export_exlib_phases src_prepare src_compile src_install

