# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache
require python [ blacklist=none ]

SUMMARY="A GObject plugins library"
HOMEPAGE="http://live.gnome.org/Libpeas"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"

FILTERED_PYTHON3_ABIS="${PYTHON_FILTERED_ABIS/2.7 }"

MYOPTIONS="
    gtk-doc
    python_abis: ( ${FILTERED_PYTHON3_ABIS} ) [[ number-selected = at-most-one ]]
    ( linguas: an ar as ast be bg bn_IN ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur gl
               gu he hi hu id it ja kn ko lt lv ml mr nb nds nl or pa pl pt pt_BR ro ru sk sl sr
               sr@latin sv ta te tg th tr ug uk vi zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.11] )
    build+run:
        dev-libs/glib:2[>=2.38.0]
        gnome-bindings/pygobject:3[>=3.2.0][python_abis:*(-)?]
        gnome-desktop/gobject-introspection:1[>=1.39.0]
        x11-libs/gtk+:3[>=3.0.0][gobject-introspection]
"

RESTRICT="test" # requires X

src_prepare() {
    local abi
    default
    edo sed -e "s/ pkg-config/ ${PKG_CONFIG}/g" -i configure
    # Fix locale install location
    edo sed "/itlocaledir = \$(prefix)/c\itlocaledir = @localedir@" -i po/Makefile.in.in
    edo mkdir "${WORKBASE}/PYTHON_ABIS"
    for abi in ${PYTHON_FILTERED_ABIS}; do
        edo ln -s "${WORK}" "${WORKBASE}/PYTHON_ABIS/${abi}"
    done
}

src_configure() {
    PYTHON_OPTIONS=()
    if optionq python_abis:2.7; then
        PYTHON_OPTIONS+=(
            '--enable-python2'
            "PYTHON2=/usr/$(exhost --target)/bin/python2.7"
            "PYTHON2_CONFIG=/usr/$(exhost --target)/bin/python2.7-config"
        )
    else
        PYTHON_OPTIONS+=( '--disable-python2' )
    fi

    for so in ${FILTERED_PYTHON3_ABIS}; do
        optionq python_abis:$so && PYTHON3_SO=$so
    done
    if [[ -n $PYTHON3_SO ]]; then
        PYTHON_OPTIONS+=(
            '--enable-python3'
            "PYTHON=/usr/$(exhost --target)/bin/python$PYTHON3_SO"
            "PYTHON3_CONFIG=/usr/$(exhost --target)/bin/python${PYTHON3_SO}-config"
        )
    else
        PYTHON_OPTIONS+=( '--disable-python3' )
    fi
    econf --disable-glade-catalog --enable-gtk \
          --disable-lua5.1 --disable-luajit \
          $(option_enable gtk-doc) \
          "${PYTHON_OPTIONS[@]}"
}

