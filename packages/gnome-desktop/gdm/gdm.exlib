# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require systemd-service pam
require gsettings
require gtk-icon-cache
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

export_exlib_phases src_prepare src_install pkg_postinst pkg_postrm

SUMMARY="GTK+ based login manager"
HOMEPAGE="http://live.gnome.org/GDM"

LICENCES="GPL-2"
SLOT="0"

DEPENDENCIES="
    build+run:
        group/gdm
        user/gdm
        gnome-desktop/dconf
    suggestion:
        x11-server/xorg-server [[ description = [ Requires an X server ] ]]
"

gdm_src_prepare() {
    edo intltoolize --force --automake
    autotools_src_prepare
    echo "WantedBy=graphical.target" >> data/gdm.service.in
}

gdm_src_install() {
    gsettings_src_install

    ever at_least 3.10 || edo rmdir "${IMAGE}/var/gdm"

    keepdir /var/lib/gdm            # GDM_WORKING_DIR
    keepdir /var/log/gdm            # GDM_LOG_DIR
    keepdir /var/cache/gdm

    # GDM_XAUTH_DIR is created automatically
    edo rmdir "${IMAGE}"/run/{gdm/,}

    # GDM_SCREENSHOT_DIR
    keepdir /var/tmp

    edo chmod g+w "${IMAGE}"/var/lib/gdm
    edo chown gdm:gdm "${IMAGE}"/var/lib/gdm

    ever at_least 3.11 || edo rmdir "${IMAGE}"/var/lib/gdm/.config/{dconf,}

    # dconf lock directory
    keepdir /etc/dconf/db/gdm.d/locks

    edo rmdir "${IMAGE}/var/lib/gdm/.local/share/applications"  \
              "${IMAGE}/var/lib/gdm/.local/share"               \
              "${IMAGE}/var/lib/gdm/.local"

    ever at_least 3.8 || install_systemd_files

    install_openrc_files
}

gdm_pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    edo dconf update
}

gdm_pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    edo dconf update
}

