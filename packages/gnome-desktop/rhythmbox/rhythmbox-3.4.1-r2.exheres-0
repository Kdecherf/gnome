# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop
require python [ blacklist="2 3.1" min_versions="3.2.3" with_opt=true multibuild=false ]

SUMMARY="An integrated music management application"
DESCRIPTION="
Rhythmbox is your one-stop multimedia application, supporting a music library,
multiple playlists, internet radio, and more.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    brasero [[ description = [ Add support for audio CD mastering via brasero ] ]]
    daap
    grilo
    gtk-doc
    ipod [[ description = [ Enable support for iPods ] ]]
    keyring
    libnotify
    mtp [[ description = [ Enable support for Microsoft's Media Transfer Protocol (MTP) ] ]]
    visualizer [[ description = [ Build visualizer plugin ] ]]
    (
        linguas:
            af am ar as az be be@latin bg bn_IN br bs ca ca@valencia cs cy da de
            dz el en_CA en_GB eo es et eu fa fi fr ga gd gl gu he hi hr hu id is
            it ja kk kn ko lt lv mk ml mn mr ms nb nds ne nl nn oc or pa pl ps
            pt pt_BR ro ru rw si sk sl sr sr@latin sv ta te th tr uk vi zh_CN
            zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        gnome-desktop/yelp-tools
        virtual/pkg-config[>=0.20]
        x11-proto/xorgproto
        gtk-doc? ( dev-doc/gtk-doc[>=1.4] )
    build+run:
        core/json-glib
        dev-db/tdb[>=1.2.6]
        dev-libs/glib:2[>=2.36.0]
        dev-libs/libpeas:1.0[>=0.7.3]
        dev-libs/libxml2:2.0[>=2.7.8]
        gnome-desktop/gobject-introspection:1[>=0.10.0]
        gnome-desktop/libsoup:2.4[>=2.42.0]
        gnome-desktop/totem-pl-parser[>=3.2.0]
        media-libs/gstreamer:1.0[>=1.0.0][gobject-introspection]
        media-plugins/gst-plugins-base:1.0[>=1.0.0][gobject-introspection]
        media-plugins/gst-plugins-good:1.0 [[ note = [ for auto audio sink ] ]]
        sys-apps/systemd[>=143] [[ note = [ for gudev ] ]]
        x11-libs/gdk-pixbuf:2.0[>=2.18.0][gobject-introspection]
        x11-libs/gtk+:3[>=3.16.0][gobject-introspection]
        x11-libs/libX11
        brasero? ( gnome-desktop/brasero:3[>=2.31.5] )
        daap? ( media-libs/libdmapsharing:3.0[>=2.9.19] )
        grilo? ( gnome-desktop/grilo:0.3[>=0.3.0] )
        ipod? ( media-libs/libgpod[>=0.7.96] )
        keyring? ( dev-libs/libsecret:1[>=0.18] )
        libnotify? ( x11-libs/libnotify[>=0.7.0] )
        mtp? ( media-libs/libmtp[>=0.3.0] )
        python? (
            dev-libs/libpeas:1.0[python_abis:*(-)?]
            gnome-bindings/pygobject:3[>=3.0.0][python_abis:*(-)?]
        )
        visualizer? (
            media-libs/clutter-gst:2.0
            x11-libs/clutter-gtk:1.0[>=1.0]
            x11-libs/clutter:1[>=1.8]
            x11-libs/mx:1.0[>=1.0.1]
        )
    test:
        dev-libs/check
"

RESTRICT="test" # requires X

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--without-hal'
    '--disable-lirc'
    '--disable-vala'
    '--disable-fm-radio'
    '--disable-browser-plugin'
    '--with-gudev'
    '--enable-mmkeys'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'brasero'
    'ipod'
    'keyring libsecret'
    'mtp'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gtk-doc'
    'daap'
    'grilo'
    'libnotify'
    'python'
    'visualizer'
)

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

