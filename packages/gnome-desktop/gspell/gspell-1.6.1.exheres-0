# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true vala_dep=true ]

SUMMARY="Spell-checking library for GTK+"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk-doc
    gobject-introspection
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.25] )
    build+run:
        app-spell/enchant[>=1.6.0]
        app-text/iso-codes[>=0.35]
        dev-libs/glib:2[>=2.44]
        sys-devel/gettext[>=0.19.4]
        x11-libs/gtk+:3[>=3.20.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.42.0] )
"

BUGS_TO="keruspe@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-installed-tests )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
    'gobject-introspection introspection'
    'vapi vala'
)

# Require access to X/Wayland
RESTRICT="test"

