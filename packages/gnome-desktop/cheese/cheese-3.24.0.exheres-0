# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache
require vala [ vala_dep=true ]

SUMMARY="Take photos and videos with your webcam, with fun graphical effects"
HOMEPAGE="http://projects.gnome.org/cheese"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection gtk-doc
    ( linguas: fa hi km zh_CN lv mr eo kk el en@shaw ta tr ca@valencia da pa af de sq ka pt fr cs ms
               sk mk hr en_GB nn zu nl it sl oc ja vi be as be@latin te fi ps id th sr@latin dz ast
               ro tg zh_HK nds kn zh_TW bn an gl sv gu or ky es lt xh ku sr uk bn_IN ml ga pt_BR pl
               et mai ru ar bg eu he hu fur ca nb ug ko )
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.3
        dev-libs/appstream-glib
        dev-libs/libxml2:* [[ note = [ from YELP_HELP_INIT in configure.ac, only needs xmllint ] ]]
        dev-util/intltool[>=0.50.0]
        dev-util/itstool [[ note = [ from YELP_HELP_INIT in configure.ac ] ]]
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.24]
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/glib:2[>=2.39.90]
        gnome-desktop/gnome-desktop:3.0
        media-libs/clutter-gst:3.0[>=3.0.0]
        media-libs/gstreamer:1.0[gobject-introspection?]
        media-libs/libcanberra[>=0.26][providers:gtk3]
        media-plugins/gnome-video-effects
        media-plugins/gst-plugins-base:1.0
        media-plugins/gst-plugins-bad:1.0[>=1.4]
        x11-libs/clutter:1[>=1.13.2][gobject-introspection?][?X]
        x11-libs/clutter-gtk:1.0
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.13.4][gobject-introspection?]
        x11-libs/libX11
        x11-libs/libXtst
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
"

RESTRICT="test" # needs X

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--disable-schemas-compile'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
)

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

