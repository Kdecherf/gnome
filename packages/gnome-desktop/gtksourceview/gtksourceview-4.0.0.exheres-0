# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix="tar.xz" ]
require vala [ vala_dep=true with_opt=true ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="Text widget with syntax highlighting support"
HOMEPAGE="https://www.gnome.org/"

LICENCES="LGPL-2.1"
SLOT="4.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="glade [[ description = [ build and install glade catalog ] ]]
    gobject-introspection gtk-doc
    ( linguas: ar as ast az be bg bn bn_IN bs ca ca@valencia cs cy da de dz el en_CA en_GB en@shaw
               eo es et eu fa fi fr ga gl gu he hi hr hu id it ja kn ko lt lv mai mg mk ml mn mr ms
               nb ne nl nn oc or pa pl pt pt_BR ro ru rw si sk sl sq sr sr@latin sv ta te tg th tr
               ug uk vi xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/itstool
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.25] )
    build+run:
        dev-libs/glib:2[>=2.48.0]
        dev-libs/libxml2:2.0[>=2.6.0]
        x11-libs/gtk+:3[>=3.20.0]
        glade? ( dev-util/glade[>=3.9] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.42.0] )
"

RESTRICT="test" # requires X

DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-deprecations' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gtk-doc' 'glade glade-catalog'
                                       'gobject-introspection introspection' 'vapi vala' )

AT_M4DIR=( m4 )

src_prepare() {
    edo rm m4/{pkg,gtk-3.0}.m4
    autotools_src_prepare
    edo touch po/stamp-it
}

