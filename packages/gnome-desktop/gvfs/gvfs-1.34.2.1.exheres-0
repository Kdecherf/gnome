# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="VFS interfaces for GTK+"
HOMEPAGE="http://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    afc [[ description = [ add support for accessing AFC (Apple File Connection) filesystems ] ]]
    afp [[ description = [ add support for accessing AFP (Apple Filing Protocol) shares ] ]]
    archive [[ description = [ add support for reading compressed archives supported by libarchive ] ]]
    avahi
    bluray [[ description = [ Blu-ray disc metadata support ] ]]
    cdda
    gphoto2
    doc fuse
    http [[ description = [ enable support http, dav, ssl uris ] ]]
    keyring
    monitor [[ description = [ add support for gnome-disk-utility ] ]]
    mtp [[ description = [ build MTP volume monitor (e.g. Android tablets and phones) ] ]]
    nfs [[ description = [ add support for accessing NFS shares ] ]]
    online-accounts [[ description = [ build online accounts volume monitor ] ]]
    samba
    udisks [[ description = [ add support for udisks based volume monitoring ] ]]
    ( linguas: af ar as ast be be@latin bg bn bn_IN ca ca@valencia cs da de el en_GB en@shaw eo es
               et eu fa fi fr ga gl gu he hi hu id it ja kk kn ko ku lt lv mai mk ml mr nb nds nl
               nn or pa pl pt pt_BR ro ru sk sl sq sr sr@latin sv ta te tg th tr ug uk vi zh_CN
               zh_HK zh_TW )
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc
               dev-libs/libxslt )
    build+run:
        dev-libs/glib:2[>=2.51.0]
        dev-libs/libusb:1[>=1.0.21]
        dev-libs/libxml2:2.0
        gnome-desktop/gcr
        gnome-desktop/libgudev[>=147]
        net-misc/openssh
        sys-auth/polkit
        sys-libs/libcap
        afc? ( app-pda/libimobiledevice[>=1.2]
               dev-libs/libplist[>=0.15] )
        afp? ( dev-libs/libgcrypt[>=1.2.2] )
        archive? ( app-arch/libarchive )
        avahi? ( net-dns/avahi[>=0.6][dbus] )
        bluray? ( media-libs/libbluray )
        cdda? ( dev-libs/libcdio[>=0.78.2]
                dev-libs/libcdio-paranoia )
        fuse? ( sys-fs/fuse:0[>=2.8.0] )
        gphoto2? ( media-libs/libgphoto2[>=2.4.10] )
        http? ( gnome-desktop/libsoup:2.4[>=2.42.0] )
        keyring? ( dev-libs/libsecret:1 )
        monitor? ( dev-libs/expat
                   gnome-desktop/gnome-disk-utility[>=3.0.2] )
        mtp? ( media-libs/libmtp[>=1.1.12] [[ note = [ permit on-device reading ] ]] )
        nfs? ( net-fs/libnfs[>=1.9.8] )
        online-accounts? (
            gnome-desktop/gnome-online-accounts[>=3.17.1]
            gnome-desktop/libgdata[>=0.17.9][online-accounts]
        )
        providers:elogind? ( sys-auth/elogind[>=44] )
        providers:systemd? ( sys-apps/systemd[>=44] )
        samba? ( net-fs/samba[>=3.4.0] )
        udisks? ( sys-apps/udisks:2[>=1.97] )
"
#    test:
#        dev-lang/python:3.2

RESTRICT="test" # requires python:3

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/0001-gvfs-exherbo-specific-systemd-path-changes.patch"
    "${FILES}/0013-Enable-support-for-elogind-substituting-systemd-logi.patch"
)

DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-admin' '--enable-libusb' '--enable-udev' '--enable-gudev' '--enable-gcr' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'afc' 'afp' 'archive' 'avahi'
                                       'bluray' 'cdda' 'doc gtk-doc'
                                       'doc documentation' 'fuse' 'gphoto2' 'http'
                                       'keyring' 'monitor gdu' 'nfs' 'online-accounts goa'
                                       'online-accounts google' 'samba' 'providers:elogind libelogind'
                                       'providers:systemd libsystemd-login' 'udisks udisks2' 'mtp libmtp' )

src_prepare() {
    edo sed -i "/AC_PATH_PROG(PKG_CONFIG/d" configure.ac
    edo autopoint
    autotools_src_prepare
}

src_install() {
    default

    ( option avahi || option samba ) || edo rmdir "${IMAGE}"/usr/share/GConf/{gsettings,}

    keepdir /usr/share/gvfs/remote-volume-monitors
}

pkg_preinst() {
    # We formerly had a keepdir on /media, and systemd mounts it as a tmpfs, so we cannot let gvfs
    # remove it and recreate it in postinst or the "rmdir /media" in the merge phase would fail.
    # Create a temporary file so that /media doesn't get unmerged, and remove it in postinst
    edo touch "${ROOT}"/media/.keep-the-media-directory-please
}

pkg_postinst() {
    gsettings_pkg_postinst
    nonfatal edo rm "${ROOT}"/media/.keep-the-media-directory-please
}

