# Copyright 2013-2014 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="A collection of plugins for the Grilo framework."
HOMEPAGE="https://wiki.gnome.org/Grilo"

LICENCES="LGPL-2.1"
SLOT="0.3"
PLATFORMS="~amd64"
MYOPTIONS="bookmarks dmap
    freebox [[ description = [ Add support for Freebox (French TV Service) ] ]]
    online-accounts tracker
    thetvdb
    ( linguas: ca ca@valencia cs da de el eo es fr gl he hu id it ja ko lt lv ml nb pl pt pt_BR ru
               sk sl sr sr@latin uk zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/gperf
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext
        sys-devel/libtool
    build+run:
        app-arch/libarchive
        core/json-glib
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.44]
        dev-libs/liboauth
        dev-libs/libxml2:2.0
        media-libs/libmediaart:2.0
        net-utils/gmime:3.0
        gnome-desktop/grilo:0.3[>=0.3.1]
        gnome-desktop/libgdata[>=0.9.1]
        gnome-desktop/totem-pl-parser[>=3.4.1]
        gnome-desktop/yelp-tools
        bookmarks? ( dev-libs/gom:1.0[>=0.3.2] )
        dmap? ( media-libs/libdmapsharing:3.0[>=2.9.12] )
        freebox? ( net-dns/avahi[gtk3] )
        online-accounts? ( gnome-desktop/gnome-online-accounts[>=3.7.91] )
        thetvdb? ( dev-libs/gom:1.0[>=0.3.2] )
        tracker? ( app-pim/tracker:=[>=0.10.5] )
"

BUGS_TO="keruspe@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-dleyna'
    '--enable-filesystem'
    '--enable-flickr'
    '--enable-gravatar'
    '--enable-jamendo'
    '--enable-local-metadata'
    '--enable-magnatune'
    '--enable-metadata-store'
    '--enable-opensubtitles'
    '--enable-optical-media'
    '--enable-podcasts'
    '--enable-raitv'
    '--enable-shoutcast'
    '--enable-tmdb'
    '--enable-vimeo'
    '--enable-youtube'

    '--disable-chromaprint'
    '--disable-lua-factory'
    '--disable-upnp'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'bookmarks' 'dmap' 'freebox' 'online-accounts goa' 'thetvdb' 'tracker' )

# Require the lua factory
RESTRICT=test

src_prepare() {
    default
    edo intltoolize --force --automake
}

