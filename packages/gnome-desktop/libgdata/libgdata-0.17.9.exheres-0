# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="GLib-based library for accessing online service APIs using the GData protocol"
DESCRIPTION="
libgdata is a GLib-based library for accessing online service APIs using the
GData protocol --- most notably, Google's services. It provides APIs to access
the common Google services, and has full asynchronous support.
"
HOMEPAGE="https://wiki.gnome.org/Projects/libgdata"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc
    gnome [[ description = [ Support for transparent proxy and non-pageable memory through GNOME ] ]]
    online-accounts [[ requires = gnome ]]
    vapi [[ requires = gobject-introspection ]]
    ( linguas: ar as be bg bn_IN ca ca@valencia cs da de el en_GB eo es et eu fi fr gl gu he hi hu
               id it ja kn ko lt lv ml mr nb nl or pa pl pt pt_BR ro ru sk sl sr sr@latin sv ta te
               tg th tr ug uk vi zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.25] )
    build+run:
        core/json-glib[>=0.15]
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libxml2:2.0
        dev-libs/liboauth[>=0.9.4]
        gnome-desktop/libsoup:2.4[>=2.42.0][gobject-introspection?]
        x11-libs/gdk-pixbuf:2.0[>=2.14] [[ note = [ automagic dependency ] ]]
        x11-libs/gtk+:3[>=2.91.2]
        gnome? ( gnome-desktop/gcr )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.7] )
        online-accounts? ( gnome-desktop/gnome-online-accounts[>=3.8] )
        vapi? ( dev-lang/vala:* )
"
# build+test:
#         dev-libs/uhttpmock[>=0.5.0]

DEFAULT_SRC_CONFIGURE_PARAMS=( '--disable-always-build-tests' '--disable-static' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gnome' 'gobject-introspection introspection'
                                       'gtk-doc' 'online-accounts goa' 'vapi vala' )

RESTRICT="test" # requires internet connections

