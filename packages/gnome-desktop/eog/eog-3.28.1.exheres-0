# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop
require option-renames [ renames=[ 'cms lcms' ] ]
require meson [ meson_minimum_version=0.44 ]

SUMMARY="Eye of GNOME is the GNOME image viewer"
HOMEPAGE="https://projects.gnome.org/eog"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    exif gobject-introspection gtk-doc lcms svg xmp
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.7]
        sys-libs/zlib
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.16] )
    build+run:
        dev-libs/glib:2[>=2.53.4]
        dev-libs/libpeas:1.0[>=0.7.4]
        gnome-desktop/gnome-desktop:3.0[>=2.91.2]
        gnome-desktop/gsettings-desktop-schemas[>=2.91.92]
        x11-libs/gdk-pixbuf:2.0[>=2.36.5]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection?]
        x11-libs/libX11
        x11-misc/shared-mime-info[>=0.20]
        exif? ( media-libs/libexif[>=0.6.14] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
        lcms? ( media-libs/lcms2 )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=8] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        svg? ( gnome-desktop/librsvg:2[>=2.36.2] )
        xmp? ( media-libs/exempi:2.0[>=1.99.5] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dlibjpeg=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'exif libexif'
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'lcms cms'
    'svg librsvg'
    xmp
)

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

