# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require python [ has_bin=true blacklist="3" min_versions="2.3.5" ] gnome.org

SUMMARY="Python bindings for GTK+"
HOMEPAGE="http://www.pygtk.org"

LICENCES="LGPL-2.1"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc glade"

UPSTREAM_DOCUMENTATION="
    https://wiki.gnome.org/Projects/PyGTK [[
        description = [ Wiki providing useful resources ]
        lang = en
    ]]
    ${HOMEPAGE}/tutorial.html [[
        description = [ Tutorial ]
        lang = en
    ]]
    https://wiki.gnome.org/Projects/PyGTK/QuickStart [[
        description = [ Quickstart Guide ]
        lang = en
    ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        doc? ( dev-libs/libxslt )
    build+run:
        dev-libs/atk[>=1.12.0]
        dev-libs/glib:2[>=2.8]
        dev-python/pycairo[>=1.1.7][python_abis:*(-)?] [[ note = [ Automagic. Needs patch to optionalize ] ]]
        gnome-bindings/pygobject:2[>=2.21.3][python_abis:*(-)?]
        x11-libs/gtk+:2[>=2.24]
        x11-libs/pango[>=1.16]
        glade? ( gnome-platform/libglade:2[>=2.5] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-numpy )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc docs' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( glade )

RESTRICT="test"

src_prepare() {
    default

    edo sed -e 's/pkg-config/\$PKG_CONFIG/' \
            -i configure
}

compile_one_multibuild() {
    python_disable_pyc

    default
}

install_one_multibuild() {
    default

    python_bytecompile

    # fix shebang
    edo sed -i -e "s:#! python2.7:#!/usr/bin/env python2:" "${IMAGE}"/usr/$(exhost --target)/bin/pygtk-demo
}

