# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="C++ bindings for glib"
HOMEPAGE="http://www.gtkmm.org"

LICENCES="LGPL-2.1"
SLOT="2.4"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    disable-deprecated [[ description = [ Omit deprecated API from the library ] ]]
"

UPSTREAM_DOCUMENTATION="
    http://library.gnome.org/devel/glibmm/stable/ [[
        lang = en
        description = [ Reference manual ]
    ]]
    http://library.gnome.org/devel/glibmm/stable/deprecated.html [[
        lang = en
        description = [ Deprecated API list ]
    ]]
"

# Tests are broken
# ../build/test-driver: the following mandatory options are missing: --test-name
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        doc? ( dev-lang/perl:*[>=5.6.0] )
    build+run:
        dev-cpp/libsigc++:2[>=2.9.1]
        dev-libs/glib:2[>=2.50.0]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc documentation'
    '!disable-deprecated deprecated-api'
)

src_prepare() {
    # Needs internet access (DNS), automake fails.
    edo sed -e '/giomm_tls_client\/test/d' -i tests/Makefile.in

    default
}

